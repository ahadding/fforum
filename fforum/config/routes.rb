Fforum::Application.routes.draw do
  resources :user_levels

  resources :boards do
    resources :board_threads, only: [:index, :new, :create]
    resources :user_board_permissions, only: [:index, :new, :create]
  end
  match '/boards/:id/marked/threads', to: 'boards#marked_threads', via: 'get',
    as: 'board_marked_threads'
  match '/boards/:id/marked/comments', to: 'boards#marked_comments', via: 'get',
    as: 'board_marked_comments'

  resources :board_threads, only: [:show, :edit, :update, :destroy] do
    resources :thread_comments, only: [:index, :new, :create]
  end
  match '/board_threads/:id/report', to: 'board_threads#report', via: 'get',
    as: 'report_board_thread'
  match '/board_threads/:id/mark', to: 'board_threads#mark', via: 'get',
    as: 'mark_board_thread'
  match '/board_threads/:id/add_review', to: 'board_threads#add_review', via: 'get',
    as: 'add_review_board_thread'

  resources :thread_comments, only: [:show, :edit, :update, :destroy]
  match 'thread_comments/:id/report', to: 'thread_comments#report', via: 'get',
    as: 'report_thread_comment'
  match 'thread_comments/:id/mark', to: 'thread_comments#mark', via: 'get',
    as: 'mark_thread_comment'
  match 'thread_comments/:id/add_review', to: 'thread_comments#add_review', via: 'get',
    as: 'add_review_thread_comment'
  
  resources :user_board_permissions, only: [:show, :edit, :update, :destroy]

  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  match '/signup', to: 'users#new', via: 'get'
  match '/signin', to: 'sessions#new', via: 'get'
  match '/signout', to: 'sessions#destroy', via:'get'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: 'boards#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

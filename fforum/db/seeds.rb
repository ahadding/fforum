# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

100.times do |i|
  user = User.new
  user.name = "SeedUser#{i}"
  user.email = "Seed#{i}@seedsss"
  user.password = 'seedpass'
  user.password_confirmation = 'seedpass'
  user.user_level_id = 2
  user.save
end
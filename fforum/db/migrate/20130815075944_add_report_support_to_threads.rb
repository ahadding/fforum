class AddReportSupportToThreads < ActiveRecord::Migration
  def change
    add_column :board_threads, :reported_number, :integer, default: 0, null: false
    add_column :board_threads, :reviewed_number, :integer, default: 0, null: false
    add_column :board_threads, :hidden, :boolean, default: false, null: false
    add_column :board_threads, :locked, :boolean, default: false, null: false
  end
end

class AddUserBoards < ActiveRecord::Migration
  def change
    add_column :boards, :user_id, :integer
    add_column :boards, :publicly_listed, :boolean, default: true, null: false
    add_column :boards, :public_read, :boolean, default: true, null: false
    add_column :boards, :public_thread_create, :boolean, default: true, null: false
    add_column :boards, :public_thread_reply, :boolean, default: true, null: false
  end
end

class SetDefaultUserLevelInUsers < ActiveRecord::Migration
  def change
    change_column :users, :user_level_id, :integer, default: 2, null: false
  end
end

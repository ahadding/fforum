class CreateBoardThreads < ActiveRecord::Migration
  def change
    create_table :board_threads do |t|
      t.belongs_to :board
      t.belongs_to :user
      t.string :title
      t.string :link

      t.timestamps
    end
  end
end

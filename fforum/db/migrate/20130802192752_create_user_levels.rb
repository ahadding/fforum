class CreateUserLevels < ActiveRecord::Migration
  def change
    create_table :user_levels do |t|
      t.string :name
      t.integer :level
      t.text :description

      t.timestamps
    end

    add_index :user_levels, :level, { unique: true }
    add_index :user_levels, :name, { unique: true }
  end
end

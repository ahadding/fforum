class AddReportSupportToComments < ActiveRecord::Migration
  def change
    add_column :thread_comments, :reported_number, :integer, default: 0, null: false
    add_column :thread_comments, :reviewed_number, :integer, default: 0, null: false
    add_column :thread_comments, :hidden, :boolean, default: false, null: false
  end
end

class CreateUserBoardPermissions < ActiveRecord::Migration
  def change
    create_table :user_board_permissions do |t|
      t.belongs_to :board, null: false
      t.belongs_to :user, null: false
      t.boolean :can_read, default: true, null: false
      t.boolean :can_post_thread, default: true, null: false
      t.boolean :can_post_comment, default: true, null: false
      t.boolean :is_admin, default: false, null: false
      t.boolean :is_moderator, default: false, null: false

      t.timestamps
    end
  end
end

class AddUserLevelToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_level, :integer, null: false, default: 20
  end
end

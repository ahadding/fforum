class CreateThreadComments < ActiveRecord::Migration
  def change
    create_table :thread_comments do |t|
      t.belongs_to :user
      t.belongs_to :board_thread
      t.text :content

      t.timestamps
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130815083639) do

  create_table "board_threads", force: true do |t|
    t.integer  "board_id"
    t.integer  "user_id"
    t.string   "title"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reported_number", default: 0,     null: false
    t.integer  "reviewed_number", default: 0,     null: false
    t.boolean  "hidden",          default: false, null: false
    t.boolean  "locked",          default: false, null: false
  end

  create_table "boards", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "publicly_listed",      default: true, null: false
    t.boolean  "public_read",          default: true, null: false
    t.boolean  "public_thread_create", default: true, null: false
    t.boolean  "public_thread_reply",  default: true, null: false
  end

  create_table "thread_comments", force: true do |t|
    t.integer  "user_id"
    t.integer  "board_thread_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reported_number", default: 0,     null: false
    t.integer  "reviewed_number", default: 0,     null: false
    t.boolean  "hidden",          default: false, null: false
  end

  create_table "user_board_permissions", force: true do |t|
    t.integer  "board_id",                         null: false
    t.integer  "user_id",                          null: false
    t.boolean  "can_read",         default: true,  null: false
    t.boolean  "can_post_thread",  default: true,  null: false
    t.boolean  "can_post_comment", default: true,  null: false
    t.boolean  "is_admin",         default: false, null: false
    t.boolean  "is_moderator",     default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_levels", force: true do |t|
    t.string   "name"
    t.integer  "level"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_levels", ["level"], name: "index_user_levels_on_level", unique: true, using: :btree
  add_index "user_levels", ["name"], name: "index_user_levels_on_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token"
    t.integer  "user_level_id",   default: 2, null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["name"], name: "index_users_on_name", unique: true, using: :btree
  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree

end

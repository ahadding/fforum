require 'test_helper'

class ThreadCommentsControllerTest < ActionController::TestCase
=begin
  setup do
    @thread_comment = thread_comments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:thread_comments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create thread_comment" do
    assert_difference('ThreadComment.count') do
      post :create, thread_comment: {  }
    end

    assert_redirected_to thread_comment_path(assigns(:thread_comment))
  end

  test "should show thread_comment" do
    get :show, id: @thread_comment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @thread_comment
    assert_response :success
  end

  test "should update thread_comment" do
    patch :update, id: @thread_comment, thread_comment: {  }
    assert_redirected_to thread_comment_path(assigns(:thread_comment))
  end

  test "should destroy thread_comment" do
    assert_difference('ThreadComment.count', -1) do
      delete :destroy, id: @thread_comment
    end

    assert_redirected_to thread_comments_path
  end
=end
end

require 'test_helper'

class UserBoardPermissionsControllerTest < ActionController::TestCase
=begin
  setup do
    @user_board_permission = user_board_permissions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_board_permissions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_board_permission" do
    assert_difference('UserBoardPermission.count') do
      post :create, user_board_permission: {  }
    end

    assert_redirected_to user_board_permission_path(assigns(:user_board_permission))
  end

  test "should show user_board_permission" do
    get :show, id: @user_board_permission
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_board_permission
    assert_response :success
  end

  test "should update user_board_permission" do
    patch :update, id: @user_board_permission, user_board_permission: {  }
    assert_redirected_to user_board_permission_path(assigns(:user_board_permission))
  end

  test "should destroy user_board_permission" do
    assert_difference('UserBoardPermission.count', -1) do
      delete :destroy, id: @user_board_permission
    end

    assert_redirected_to user_board_permissions_path
  end
=end
end

require 'test_helper'

class UserPolicyTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'edit? should be true for users that edit themselves' do
    user = users :valid
    assert UserPolicy.edit?(user, user)
  end

  test 'edit? true when admin is current user' do
    user = users :admin
    other = users :valid
    assert UserPolicy.edit?(user, other)
  end

  test 'edit? false when neither admin is current user, nor current user editing self' do
    user = users :valid
    other = users :admin
    assert !(UserPolicy.edit?(user, other))
  end
end
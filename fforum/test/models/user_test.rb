require 'test_helper'
require 'shoulda'
require 'shoulda-matchers'

class UserTest < ActiveSupport::TestCase
  
  # Returns a user from the users fixture, but sets their password and
  # password_confirmation fields so that they will not fail every validation
  # just for not having them,
  # as fixtures don't support passwords for has_secure_password.
  def set_valid_password_conf_user(user_fixture_name_symbol)
    user = users user_fixture_name_symbol
    user.password = 'testestest'
    user.password_confirmation = 'testestest'
    user
  end

  test 'board_admin? true when user is admin of given board' do
    user = users :admin_of_board_99
    board = boards :board99
    assert user.board_admin? board
  end

  test 'board_admin? false when user is not admin of given board' do
    user = users :valid
    board = boards :board99
    assert !(user.board_admin? board)
  end

  test 'board_moderator? true when user is moderator of given board' do
    user = users :mod_of_board_99
    board = boards :board99
    assert user.board_moderator? board
  end

  test 'board_moderator? false when user is not moderator of given board' do
    user = users :valid
    board = boards :board99
    assert !(user.board_moderator? board)
  end

  test 'should respond to .user_level' do
    user = set_valid_password_conf_user :valid
    assert user.respond_to?(:user_level), 'should respond to user_level'
  end

  test 'invalid if user_level is nil' do
    user = set_valid_password_conf_user :valid
    user.user_level = nil
    assert user.invalid?, 'should be invalid since user has no level'
  end

  test 'invalid if user_level does not exist in db' do
    user = set_valid_password_conf_user :valid
    user.user_level_id = 52345
    assert user.invalid?, 'should be invalid since user_level does not exist in db'
  end

  test 'admin? true if user has an admin user level' do
    user = set_valid_password_conf_user :admin
    assert user.admin?, 'should be true as user is admin'
  end

  test 'admin? false if user does not have an admin user level' do
    user = set_valid_password_conf_user :probated
    assert !user.admin?, 'should be false as user is not an admin'
  end

  test 'probated? false if user not probated' do
    user = set_valid_password_conf_user :admin
    assert !user.probated?, 'should be false use is not probated'
  end

  test 'probated? true if user has probated user level' do
    user = set_valid_password_conf_user :probated
    assert user.probated?, 'should be true since user is probated'
  end

  # TODO:
  # Write more tests to ensure the same validations are run when Users are
  # edited.
  test 'invalid if name less than 1 character' do
    user = set_valid_password_conf_user(:valid)
    user.name = ""
    user.save
    assert(user.invalid?, 'name < 1 character should be invalid')
  end

  test 'invalid if name greater than 16 characters' do
    user = set_valid_password_conf_user(:valid)
    user.name = "12345678901234567"
    user.save
    assert(user.invalid?, 'name > 16 characters should be invalid')
  end

  test 'invalid if name not present' do
    user = set_valid_password_conf_user(:valid)
    user.name = nil
    user.save
    assert(user.invalid?, 'non-present name; should be invalid')
  end

  test 'invalid if name not unique' do
    user = set_valid_password_conf_user :valid
    user.name = users(:one).name
    user.save
    assert user.invalid?
  end

  test 'name should not have any non-alpha-numerica characters other than
    underscores and spaces' do
    user = set_valid_password_conf_user :valid
    user.name = '<>?"'
    user.save
    assert user.invalid?
  end

  test 'invalid if email not present' do
    user = set_valid_password_conf_user :valid
    user.email = nil
    user.save
    assert user.invalid?
  end

  test 'invalid if email not unique' do
    user = set_valid_password_conf_user :valid
    user.email = users(:one).email
    user.save
    assert user.invalid?
  end

  test 'invalid if password less than 8 characters' do
    user = set_valid_password_conf_user :valid
    user.password = '1234567'
    user.save
    assert user.invalid?
  end

  test 'invalid if password greater than 30' do
    user = set_valid_password_conf_user :valid
    user.password = '1234567890123456789012345678901234567'
    user.save
    assert user.invalid?
  end
end
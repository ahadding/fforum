require 'test_helper'

class UserLevelTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'responds to .users' do
    user_level = user_levels :admin
    assert user_level.respond_to?(:users), 'should allow access to users with this level'
  end

  test 'invalid if no name' do
    user_level = user_levels :probated
    user_level.name = nil
    assert user_level.invalid?, 'should be invalid'
  end

  test 'invalid if blank name' do
    user_level = user_levels :probated
    user_level.name = ''
    assert user_level.invalid?, 'should be invalid'
  end

  test 'invalid if no level' do
    user_level = user_levels :probated
    user_level.level = nil
    assert user_level.invalid?, 'should be invalid'
  end

  test 'invalid if level not unique' do
    user_level = user_levels :probated
    user_level.level = (user_levels :admin).level
    assert user_level.invalid?, 'should be invalid'
  end

  test 'invalid if no description' do
    user_level = user_levels :probated
    user_level.description = nil
    assert user_level.invalid?, 'should be invalid'
  end

  test 'invalid if description blank' do
    user_level = user_levels :probated
    user_level.description = ''
    assert user_level.invalid?, 'should be invalid'
  end

  test 'valid if all fields valid' do
    user_level = user_levels :admin
    assert user_level.valid?, 'should be valid'
  end

  test 'admin? true if user_level.level >= 99' do
    user_level = user_levels :admin
    assert user_level.admin?
  end

  test 'admin? false if level < 99' do
    user_level = user_levels :probated
    assert !user_level.admin?
  end

  test 'probated? true if level less than 10' do
    user_level = user_levels :probated
    assert user_level.probated?
  end

  test 'probated? false if level >= 10' do
    user_level = user_levels :admin
    assert !user_level.probated?
  end
end

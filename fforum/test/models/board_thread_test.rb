require 'test_helper'

class BoardThreadTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'valid when all fields valid, link present' do
    thread = board_threads :valid
    thread.save
    assert thread.valid?, 'all fields valid, model should be valid'
  end

  test 'valid when all fields valid, link nil' do
    thread = board_threads :valid
    thread.link = nil
    thread.save
    assert thread.valid?, 'all fields valid, model should be valid'
  end

  test 'responds to .board' do 
    thread = board_threads :valid
    assert thread.respond_to?(:board), 'should respond to .board()'
  end

  test 'responds to .thread_comments' do
    thread = board_threads :valid
    assert thread.respond_to?(:thread_comments), 'should respond to .thread_comments'
  end

  test 'responds to .user' do
    thread = board_threads :valid
    assert thread.respond_to?(:user), 'should respond to .user'
  end

  test 'invalid if title not present' do
    thread = board_threads :valid
    thread.title = nil
    thread.save
    assert thread.invalid?, 'should be invalid as title is nil'
  end

  test 'invalid if title less than 1 character' do
    thread = board_threads :valid
    thread.title = ''
    thread.save
    assert thread.invalid?, 'should be invalid as title is a blank string'
  end

  test 'invalid if title greater than 80 characters' do
    thread = board_threads :valid
    thread.title = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab'
    thread.save
    assert thread.invalid?, 'should be invalid as title is over 80 characters'
  end

  test 'invalid if user_id not present' do
    thread = board_threads :valid
    thread.user_id = nil
    thread.save
    assert thread.invalid?, 'should be invalid as user_id not present'
  end

  test 'invalid if user_id does not match an existing User' do
    thread = board_threads :valid
    thread.user_id = -1
    thread.save
    assert thread.invalid?, 'should be invalid as user_id does not match an existing User'
  end

  # when adding authorization, test that threads are invalid when user has no
  # permission to make threads in general?
  # THAT_TEST_GOES_HERE

  test 'invalid if board_id not present' do
    thread = board_threads :valid
    thread.board_id = nil
    thread.save
    assert thread.invalid?, 'should be invalid as board_id not present'
  end

  test 'invalid if board_id does not match an existing Board' do
    thread = board_threads :valid
    thread.board_id = -1
    thread.save
    assert thread.invalid?, 'should be invalid as board_id does not match an existing Board'
  end

  # when adding auth, test that threads are invalid when user has no permission
  # to make threads on given board
  # THAT_TEST_GOES_HERE

  test 'invalid if link is not a valid URL' do
    thread = board_threads :valid
    thread.link = 'sdfgssdhdgh'
    assert thread.invalid?, 'should be invalid since link is invalid'
  end
end

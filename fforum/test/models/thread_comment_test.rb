require 'test_helper'

class ThreadCommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'responds to .board_thread' do
    comment = thread_comments :valid
    assert comment.respond_to?(:board_thread), 'should be able to access containing thread'
  end

  test 'responds to .user' do
    comment = thread_comments :valid
    assert comment.respond_to?(:user), 'should be able to access owning user'
  end

  test 'model valid when all properties valid' do
    comment = thread_comments :valid
    comment.save
    assert comment.valid?, 'should be valid as all fields are valid'
  end

  test 'invalid when content is not present' do
    comment = thread_comments :valid
    comment.content = nil
    comment.save
    assert comment.invalid?, 'should be invalid as content is nil'
  end

  test 'invalid when content length < 1' do 
    comment = thread_comments :valid
    comment.content = ''
    comment.save
    assert comment.invalid?, 'should be invalid as content has length 0'
  end

  test 'invalid when content length > 1000' do 
    comment = thread_comments :valid
    comment.content = 'aaaaaaaaaa'
    (1..8).each do 
      comment.content = comment.content + comment.content
    end
    comment.save
    assert comment.invalid?, 'should be invalid as content has length > 1000'
  end

  test 'invalid if user_id not present' do
    comment = thread_comments :valid
    comment.user_id = nil
    comment.save
    assert comment.invalid?, 'should be invalid as user_id not present'
  end

  test 'invalid if user_id does not match an existing User' do
    comment = thread_comments :valid
    comment.user_id = -1
    comment.save
    assert comment.invalid?, 'should be invalid as user_id does not match an existing User'
  end

  # should we also make sure user_id matches the logged-in user?
  # also, test for authorization of user before adding that feature

  test 'invalid if board_thread_id not present' do
    comment = thread_comments :valid
    comment.board_thread_id = nil
    comment.save
    assert comment.invalid?, 'should be invalid as board_thread_id is nil'
  end

  test 'invalid if board_thread_id doesnt match an existing BoardThread' do
    comment = thread_comments :valid
    comment.board_thread_id = -1
    comment.save
    assert comment.invalid?, 'should be invalid as no board matches board_thread_id'
  end
end

require 'test_helper'

class UserBoardPermissionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'invalid if board does not exist' do
    ubp = user_board_permissions :valid
    ubp.board_id = -13423
    assert ubp.invalid?, 'should be invalid as board_id is invalid'
  end

  test 'invalid if user does not exist' do
    ubp = user_board_permissions :valid
    ubp.user_id = -912592
    assert ubp.invalid?, 'should be invalid as user_id is invalid'
  end

  test 'valid if properties really are valid' do
    ubp = user_board_permissions :valid
    assert ubp.valid?, 'should be valid since all fields contrived to be valid'
  end
end

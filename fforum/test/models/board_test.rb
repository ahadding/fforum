require 'test_helper'

class BoardTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'valid if board has valid data' do
    board = boards :valid
    board.save
    assert(board.valid?)
  end

  test 'invalid if title less than 1 character' do
    board = boards :valid
    board.title = ''
    board.save
    assert board.invalid?, 'should be invalid with title having <1 character'
  end

  test 'invalid if title not present' do
    board = boards :valid
    board.title = nil
    board.save
    assert board.invalid?, 'should be invalid with title missing'
  end

  test 'invalid if title greater than 80 characters' do
    board = boards :valid
    board.title = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab'
    assert board.invalid?, 'should be invalid with title longer than 80 characters'
  end

  test 'responds to .board_threads' do
    board = boards :valid
    assert(board.respond_to? :board_threads)
  end

  test 'responds to .thread_comments' do
    board = boards :valid
    assert(board.respond_to? :thread_comments)
  end
end

class ThreadComment < ActiveRecord::Base
  belongs_to :board_thread
  belongs_to :user

  validates :content, presence: true
  validates :content, length: { in: 1..1000 }
  validates :user_id, presence: true
  validates :board_thread_id, presence: true
  validate :validate_user_id
  validate :validate_board_thread_id

  self.per_page = 10

  private
  # TODO: refactor this along with similar code in app/models/board_thread.rb
    def validate_user_id
      if !User.exists?(self.user_id)
        errors.add :user_id, 'does not exist'
      end
    end

    def validate_board_thread_id
      if !BoardThread.exists?(self.board_thread_id)
        errors.add :board_thread_id, 'does not exist'
      end
    end
end

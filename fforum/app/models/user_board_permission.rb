class UserBoardPermission < ActiveRecord::Base
  belongs_to :board
  belongs_to :user
  validate :board_exists, :user_exists
  self.per_page = 30

  def self.get_permissions(user, board)
    UserBoardPermission.find_by user_id: user.id, board_id: board.id
  end
  
  private
    def board_exists
      unless Board.exists? self.board_id
        errors.add :board_id, 'does not exist'
      end
    end

    def user_exists
      unless User.exists? self.user_id
        errors.add :user_id, 'does not exist'
      end
    end
end

require 'uri'

class BoardThread < ActiveRecord::Base
  belongs_to :board
  belongs_to :user
  has_many :thread_comments, dependent: :destroy

  validates :title, presence: true
  validates :title, length: { in: 1..80 }
  validates :user_id, presence: true
  validates :board_id, presence: true
  validate :validate_user_id, :validate_board_id, :validate_link_formatted_or_nil

  self.per_page = 10

  private

    def validate_user_id
      unless User.exists?(self.user_id)
        errors.add :user_id, 'does not exist'
      end
    end

    def validate_board_id
      unless Board.exists?(self.board_id)
        errors.add :board_id, 'does not exist'
      end
    end

    def validate_link_formatted_or_nil
      unless self.link.nil?
        unless /\A\z/.match self.link
          errors.add :link, 'invalid URL' unless URI::regexp(%w(http https)).match self.link
        end
      end
    end
end

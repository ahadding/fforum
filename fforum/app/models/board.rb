class Board < ActiveRecord::Base
  validates :title, presence: true
  validates :title, length: { in: 1..80 }
  has_many :board_threads, dependent: :destroy
  has_many :thread_comments, through: :board_threads
  belongs_to :user
  has_many :user_board_permissions

  self.per_page = 10
end

class BadNameValidator < ActiveModel::Validator
  def validate(record)
    # Just refuse any non-word characters
    record.errors[:name] << 'Bad input in name!' if /\W/.match record.name
  end
end

class User < ActiveRecord::Base
  before_save :downcase_email
  before_create :create_remember_token
  has_many :boards
  belongs_to :user_level
  has_many :user_board_permissions

  validates :name, :email, :password, :password_confirmation, presence: true
  validates :name, :email, uniqueness: { case_sensitive: false }
  validates :name, length: { in: 1..16 }
  validates :password, length: { minimum: 8 }
  validates :password, :email, length: { maximum: 30 }
  validates_with BadNameValidator
  validates :user_level, presence: true
  validate :validate_user_level_exists
  
  has_secure_password

  self.per_page = 30

  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def admin?
    self.user_level.admin?
  end

  def probated?
    self.user_level.probated?
  end

  def board_admin?(board)
    ubp = get_user_board_permission board
    (self.id == board.user_id) || (ubp && ubp.is_admin)
  end

  def board_moderator?(board)
    ubp = get_user_board_permission board
    ubp && ubp.is_moderator
  end

  def at_least_board_moderator?(board)
    board_admin?(board) || board_moderator?(board)
  end

  def at_least_site_or_board_moderator?(board)
    admin? || board_admin?(board) || board_moderator?(board)
  end

  def board_permission(board)
    UserBoardPermission.get_permissions(self, board)
  end

  private
    def get_user_board_permission(board)
      UserBoardPermission.find_by user_id: self.id, board_id: board.id
    end

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end

    def downcase_email
      self.email = email.downcase
    end

    def validate_user_level_exists
      unless UserLevel.exists?(user_level_id)
        errors[:user_level] << 'User level does not exist in DB!'
      end
    end
end
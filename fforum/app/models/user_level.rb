class UserLevel < ActiveRecord::Base
  has_many :users

  validates :level, presence: true
  validates :level, uniqueness: true
  validates :name, presence: true
  validates :name, length: { in: 1..80 }
  validates :description, presence: true
  validates :description, length: { in: 1..400 }

  def admin?
    self.level >= 99
  end

  def probated?
    self.level < 10
  end
end

module BoardThreadsHelper
  def board_thread_page_path(board_thread, page_number)
    board_thread_path(board_thread) + "/p#{page_number}"
  end
end

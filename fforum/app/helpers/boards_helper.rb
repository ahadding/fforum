module BoardsHelper
  def board_page_path(board, page_number)
    board_path(board) + "/p#{page_number}"
  end
end

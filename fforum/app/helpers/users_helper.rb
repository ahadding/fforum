module UsersHelper
  def users_page_path(page_number)
    users_path + "/p#{page_number}"
  end
end

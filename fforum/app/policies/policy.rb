class Policy
  def self.create?
    false
  end

  def self.new?
    false
  end

  def self.edit?
    false
  end

  def self.update?
    false
  end

  def self.show?
    false
  end

  def self.index?
    false
  end

  def self.destroy?
    false
  end
end
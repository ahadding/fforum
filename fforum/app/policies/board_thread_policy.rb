class BoardThreadPolicy < Policy
  def self.new?(current_user, board)
    BoardPolicy.user_can_post_thread_on_board? current_user, board
  end

  def self.create?(current_user, board)
    new? current_user, board
  end

  def self.show?(current_user, thread)
    BoardPolicy.user_can_read_board? current_user, thread.board
  end

  def self.edit?(current_user, thread)
    # forbid users that arent logged in
    return false if current_user.nil?
    current_user.at_least_site_or_board_moderator?(thread.board)
  end

  def self.update?(current_user, thread)
    edit?(current_user, thread)
  end

  def self.index?(current_user)
    false
  end

  def self.destroy?(current_user, thread)
    # forbid users that arent logged in
    return false if current_user.nil?
    current_user.admin?
  end

  def self.mark?(current_user, thread)
    # forbid users that arent logged in
    return false if current_user.nil?
    show? current_user, thread
  end

  def self.report?(current_user, thread)
    # forbid users that arent logged in
    return false if current_user.nil?
    mark? current_user, thread
  end
end
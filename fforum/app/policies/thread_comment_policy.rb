class ThreadCommentPolicy < Policy
  def self.new?(current_user, board)
    BoardPolicy.user_can_post_comment_on_board? current_user, board
  end

  def self.create?(current_user, board)
    new? current_user, board
  end

  def self.show?(current_user, comment)
    false
  end

  def self.edit?(current_user, comment)
    # forbid users that arent logged in
    return false if current_user.nil?
    current_user.at_least_site_or_board_moderator?(comment.board_thread.board)
  end

  def self.update?(current_user, comment)
    edit?(current_user, comment)
  end

  def self.index?(current_user)
    false
  end

  def self.destroy?(current_user, comment)
    # forbid users that arent logged in
    return false if current_user.nil?
    current_user.admin?
  end

  def self.mark?(current_user, comment)
    BoardThreadPolicy.mark? current_user, comment.board_thread
  end

  def self.report?(current_user, comment)
    mark? current_user, comment
  end
end
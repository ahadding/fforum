class UserBoardPermissionPolicy < Policy
  def self.new?(current_user, board, target_user)
    # forbid users that arent logged in
    return false if current_user.nil?

    target_ubp = UserBoardPermission.get_permissions target_user, board
    current_ubp = UserBoardPermission.get_permissions current_user, board

    # if permission already exists for target user / board combo, then nobody 
    # can make a new one.
    return false if target_ubp
    # otherwise, site admins and board mods/admins can make a new ubp
    current_user.admin? || current_user.at_least_board_moderator?(board)
  end

  def self.create?(current_user, board, target_user)
    new? current_user, board, target_user
  end

  def self.show?(current_user, user_board_permission)
    # forbid users that arent logged in
    return false if current_user.nil?

    current_user.admin? || current_user.at_least_board_moderator?(user_board_permission.board)
  end

  def self.edit?(current_user, user_board_permission)# forbid users that arent logged in
    return false if current_user.nil?

    if user_board_permission.is_admin || user_board_permission.is_moderator
      current_user.admin? || current_user.board_admin?(user_board_permission.board)
    else
      current_user.admin? || current_user.at_least_board_moderator?(user_board_permission.board)
    end
  end

  def self.update?(current_user, user_board_permission)
    edit?(current_user, user_board_permission)
  end

  def self.index?(current_user, board)
    # forbid users that arent logged in
    return false if current_user.nil?

    current_user.admin? || current_user.at_least_board_moderator?(board)
  end

  def self.destroy?(current_user, user_board_permission)
    # forbid users that arent logged in
    return false if current_user.nil?
    
    current_user.admin? || current_user.board_admin?(user_board_permission.board)
  end
end
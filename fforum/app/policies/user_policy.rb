class UserPolicy < Policy
  # Checks whether the current_user is authorized to edit a given user.
  # Returns true if current_user is an admin, or if given user is the current
  # user.
  def self.edit?(current_user, user_to_edit)
    # forbid users that arent logged in
    return false if current_user.nil?
    
    current_user.admin? || current_user == user_to_edit
  end

  def self.update?(current_user, user_to_update)
    edit?(current_user, user_to_update)
  end

  def self.destroy?(current_user, user_to_delete)
    # forbid users that arent logged in
    return false if current_user.nil?
    
    current_user.admin?
  end

  def self.show?(current_user, target_user)
    true
  end

  def self.new?(current_user)
    # only allow signed-in users to make a new account if theyre an admin
    return false if !current_user.nil? && !current_user.admin?
    true
  end

  def self.create?(current_user)
    new? current_user
  end

  def self.index?(current_user)
    # forbid users that arent logged in
    return false if current_user.nil?
    current_user.admin?
  end
end
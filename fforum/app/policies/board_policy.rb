class BoardPolicy < Policy
  def self.new?(current_user)
    # must be logged in to make a new board
    return false if current_user.nil?
    current_user.user_level.level >= 10
  end

  def self.create?(current_user)
    new? current_user
  end

  def self.show?(current_user, board)
    user_can_read_board? current_user, board
  end

  def self.edit?(current_user, board)
    # anon-users cant edit boards
    return false if current_user.nil?
    current_user.admin? || (board.user_id == current_user.id)
  end

  def self.update?(current_user, board)
    edit?(current_user, board)
  end

  def self.index?(current_user)
    true
  end

  def self.destroy?(current_user, board)
    # anon-users cant edit boards
    return false if current_user.nil?
    current_user.admin?
  end

  def self.marked_threads?(current_user, board)
    # forbid users that arent logged in
    return false if current_user.nil?
    current_user.at_least_site_or_board_moderator?(board)
  end

  def self.marked_comments?(current_user, board)
    # forbid users that arent logged in
    return false if current_user.nil?
    marked_threads? current_user, board
  end

  def self.user_can_read_board?(user, board)
    if !user.nil?
      # Site and local admins and mods can always read.
      return true if user.at_least_site_or_board_moderator?(board)
    else
      # anon. users can read if board is public
      return true if board.public_read
    end
    # For everyone else, use the specific ubp if one exists. otherwise, use the
    # board's default setting.
    ubp = UserBoardPermission.get_permissions user, board
    if ubp
      ubp.can_read
    else
      board.public_read
    end
  end

  def self.user_can_post_thread_on_board?(user, board)
    # must be logged in to post
    return false if user.nil?
    # Site and local admins and mods can always read.
    return true if user.at_least_site_or_board_moderator?(board)

    # For everyone else, use the specific ubp if one exists. otherwise, use the
    # board's default setting.
    ubp = UserBoardPermission.get_permissions user, board
    if ubp
      ubp.can_post_thread
    else
      board.public_thread_create && (user.user_level.level >= 10)
    end
  end

  def self.user_can_post_comment_on_board?(user, board)
    # must be logged in to post
    return false if user.nil?
    # Site and local admins and mods can always read.
    return true if user.at_least_site_or_board_moderator?(board)

    # For everyone else, use the specific ubp if one exists. otherwise, use the
    # board's default setting.
    ubp = UserBoardPermission.get_permissions user, board
    if ubp
      ubp.can_post_comment
    else
      board.public_thread_reply && (user.user_level.level >= 10)
    end
  end
end
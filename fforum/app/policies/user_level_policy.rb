class UserLevelPolicy < Policy
  def self.new?(current_user)
    # forbid users that arent logged in
    return false if current_user.nil?

    current_user.admin?
  end

  def self.create?(current_user)
    new? current_user
  end

  def self.show?(current_user, user_level)
    # forbid users that arent logged in
    return false if current_user.nil?

    current_user.admin?
  end

  def self.edit?(current_user, user_level)
    # forbid users that arent logged in
    return false if current_user.nil?

    current_user.admin?
  end

  def self.update?(current_user, user_level)
    edit?(current_user, user_level)
  end

  def self.index?(current_user)
    # forbid users that arent logged in
    return false if current_user.nil?

    current_user.admin?
  end

  def self.destroy?(current_user, user_level)
    # forbid users that arent logged in
    return false if current_user.nil?
    
    current_user.admin?
  end
end
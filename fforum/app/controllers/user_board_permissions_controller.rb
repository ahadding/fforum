class UserBoardPermissionsController < ApplicationController
  before_action :set_user_board_permission, only: [:show, :edit, :update, :destroy]
  before_action :set_board, only: [:index, :new, :create]
  before_action :set_target_user, only: [:new, :create]
  before_action :handle_checkbox_parameters, only: [:create, :update]

  # GET /user_board_permissions
  # GET /user_board_permissions.json
  def index
    if UserBoardPermissionPolicy.index?(current_user, @board)
      @user_board_permissions = UserBoardPermission.where(board_id: @board.id).paginate(page: params[:page])
    else
      redirect_to root_path
    end
  end

  # GET /user_board_permissions/1
  # GET /user_board_permissions/1.json
  def show
    redirect_to(root_path) unless UserBoardPermissionPolicy.show?(current_user,
      @user_board_permission)
  end

  # GET /user_board_permissions/new
  def new
    if UserBoardPermissionPolicy.new?(current_user, @board, @target_user)
      @user_board_permission = UserBoardPermission.new
      @form_submit_url = board_user_board_permissions_path
    else
      redirect_to root_path
    end
  end

  # GET /user_board_permissions/1/edit
  def edit
    redirect_to(root_path) unless UserBoardPermissionPolicy.edit?(current_user,
      @user_board_permission)
    @form_submit_url = nil
  end

  # POST /user_board_permissions
  # POST /user_board_permissions.json
  def create
    if UserBoardPermissionPolicy.create?(current_user, @board, @target_user)
      @user_board_permission = UserBoardPermission.new(user_board_permission_params)

      # only admin-level users may appoint admins/mods
      if current_user.admin? || current_user.board_admin?(@board)
        if params[:user_board_permission][:is_admin].nil?
          @user_board_permission.is_admin = false
        else
          @user_board_permission.is_admin = params[:user_board_permission][:is_admin]
        end

        if params[:user_board_permission][:is_moderator].nil?
          @user_board_permission.is_moderator = false
        else
          @user_board_permission.is_moderator = params[:user_board_permission][:is_moderator]
        end
      else
        @user_board_permission.is_admin = false
        @user_board_permission.is_moderator = false
      end

      @user_board_permission.board_id = @board.id
      @user_board_permission.user_id = @target_user.id

      if @user_board_permission.save
        redirect_to @user_board_permission, notice: 'User board permission was successfully created.'
      else
        render action: 'new'
      end
    else
      redirect_to root_path
    end
  end

  # PATCH/PUT /user_board_permissions/1
  # PATCH/PUT /user_board_permissions/1.json
  def update
    if UserBoardPermissionPolicy.update?(current_user, @user_board_permission)
      if current_user.admin? || current_user.board_admin?(@user_board_permission.board)
        # allow any change for an admin
        if params[:user_board_permission][:is_admin].nil?
          @user_board_permission.is_admin = false
        else
          @user_board_permission.is_admin = params[:user_board_permission][:is_admin]
        end

        if params[:user_board_permission][:is_moderator].nil?
          @user_board_permission.is_moderator = false
        else
          @user_board_permission.is_moderator = params[:user_board_permission][:is_moderator]
        end

        if @user_board_permission.update_attributes(user_board_permission_params)
          redirect_to @user_board_permission, notice: 'User board permission was successfully updated.'
        else
          render action: 'edit'
        end
      elsif !(@user_board_permission.is_admin || @user_board_permission.is_moderator)
        if @user_board_permission.update(user_board_permission_params)
          redirect_to @user_board_permission, notice: 'User board permission was successfully updated.'
        else
          render action: 'edit'
        end
      end
    else
      redirect_to root_path
    end
  end

  # DELETE /user_board_permissions/1
  # DELETE /user_board_permissions/1.json
  def destroy
    if UserBoardPermissionPolicy.destroy?(current_user, @user_board_permission)
      @user_board_permission.destroy
      redirect_to user_board_permissions_url
    else
      redirect_to root_path
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_board_permission
      @user_board_permission = UserBoardPermission.find(params[:id])
      @target_user = @user_board_permission.user
      @board = @user_board_permission.board
    end

    def set_board
      @board = Board.find(params[:board_id])
    end

    def set_target_user
      @target_user = User.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_board_permission_params
      params.require(:user_board_permission).permit(:can_read,
        :can_post_thread, :can_post_comment)
    end

    def handle_checkbox_parameters
      params[:user_board_permission].keys.each do | key |
        val = params[:user_board_permission][key]
        if val.nil?
          params[:user_board_permission][key] = false
        end
      end
    end
end
class ThreadCommentsController < ApplicationController
  before_action :set_thread_comment, only: [:show, :edit, :update, :destroy, :report, :mark]

  # GET /thread_comments
  # GET /thread_comments.json
  def index
    if ThreadCommentPolicy.index?(current_user)
      @thread_comments = ThreadComment.all
    else
      redirect_to(root_path)
    end
  end

  # GET /thread_comments/1
  # GET /thread_comments/1.json
  def show
    redirect_to(root_path) unless ThreadCommentPolicy.show?(current_user, @thread_comment)
  end

  # GET /thread_comments/new
  def new
    @board_thread = BoardThread.find(params[:board_thread_id])
    if ThreadCommentPolicy.new?(current_user, @board_thread.board)
      @thread_comment = @board_thread.thread_comments.build
      @form_submit_url = board_thread_thread_comments_path
    else
      redirect_to(root_path)
    end
  end

  # GET /thread_comments/1/edit
  def edit
    redirect_to(root_path) unless ThreadCommentPolicy.edit?(current_user, @thread_comment)
    @form_submit_url = nil
  end

  # POST /thread_comments
  # POST /thread_comments.json
  def create
    @board_thread = BoardThread.find(params[:board_thread_id])
    if ThreadCommentPolicy.create?(current_user, @board_thread.board)
      @thread_comment = ThreadComment.new(thread_comment_params)
      @thread_comment.user = current_user
      @thread_comment.board_thread = @board_thread
      @board_thread = @thread_comment.board_thread

      if @thread_comment.save
        redirect_to @board_thread, notice: 'Comment was successfully created.'
      else
        render action: 'new'
      end
    else
      redirect_to(root_path)
    end
  end

  # PATCH/PUT /thread_comments/1
  # PATCH/PUT /thread_comments/1.json
  def update
    if ThreadCommentPolicy.update?(current_user, @thread_comment)
      respond_to do |format|
        if @thread_comment.update(thread_comment_params)
          format.html { redirect_to @thread_comment.board_thread, notice: 'Thread comment was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @thread_comment.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(root_path)
    end
  end

  # DELETE /thread_comments/1
  # DELETE /thread_comments/1.json
  def destroy
    if ThreadCommentPolicy.destroy?(current_user, @thread_comment)
      @thread_comment.destroy
      respond_to do |format|
        format.html { redirect_to thread_comments_url }
        format.json { head :no_content }
      end
    else
      redirect_to(root_path)
    end
  end

  # Allow a user to confirm they want to report a comment.
  def report
    redirect_to(root_path) unless ThreadCommentPolicy.report?(current_user, @thread_comment)
  end

  # Mark a comment for moderator review.
  def mark
    if ThreadCommentPolicy.mark?(current_user, @thread_comment)
      # Some comments might have null values for reported_number.
      if @thread_comment.reported_number
        @thread_comment.reported_number += 1
      else
        @thread_comment.reported_number = 1
      end

      if @thread_comment.save
        redirect_to @thread_comment.board_thread, notice: 'Comment was successfully marked for review.'
      else
        redirect_to report_thread_comment_path(@thread_comment), notice: 'Comment not marked for review.'
      end
    else
      redirect_to(root_path)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thread_comment
      @thread_comment = ThreadComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thread_comment_params
      params.require(:thread_comment).permit(:content)
    end
end
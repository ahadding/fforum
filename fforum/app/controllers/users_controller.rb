class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :require_user_signed_in, only: [:index, :edit, :update]

  # GET /users
  # GET /users.json
  def index
    if UserPolicy.index?(current_user)
      @users = User.paginate page: params[:page]
    else
      redirect_to(root_path)
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    redirect_to(root_path) unless UserPolicy.show?(current_user, @user)
  end

  # GET /users/new
  def new
    if UserPolicy.new?(current_user)
      @user = User.new
    else
      redirect_to(root_path)
    end
  end

  # GET /users/1/edit
  def edit
    redirect_to(root_path) unless UserPolicy.edit?(current_user, @user)
  end

  # POST /users
  # POST /users.json
  def create
    if UserPolicy.create?(current_user)
      @user = User.new(user_params)

      if @user.save
        unless signed_in?
          sign_in @user
          redirect_to root_path, notice: 'Congratulations, your account has been created!'
        else
          redirect_to @user, notice: 'User was successfully created.'
        end
      else
        render 'new'
      end
    else
      redirect_to(root_path)
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if UserPolicy.update?(current_user, @user)
      @user.assign_attributes user_params

      # if current_user is an admin, we'll also update the user_level attribute
      if current_user.admin?
        user_level_id = params[:user_level][:user_level_id]
        @user.user_level_id = user_level_id
      end

      if @user.save
        flash.now[:success] = 'Updated profile successfully!'
        redirect_to @user, notice: 'User was successfully updated.'
      else
        render 'edit'
      end
    else
      redirect_to(root_path)
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if UserPolicy.destroy?(current_user, @user)
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url }
        format.json { head :no_content }
      end
    else
      redirect_to(root_path)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def require_user_signed_in
      unless signed_in?
        redirect_to signin_url, notice: "Please sign in."
      end
    end

    def require_correct_user
      redirect_to(root_path) unless current_user?(@user)
    end
end

class BoardThreadsController < ApplicationController
  before_action :set_board_thread, only: [:show, :edit, :update, :destroy, :report, :mark]

  # GET /board_threads
  # GET /board_threads.json
  def index
    if BoardThreadPolicy.index?(current_user)
      @board_threads = BoardThread.all
    else
      redirect_to(root_path)
    end
  end

  # GET /board_threads/1
  def show
    # redirect to root if user has no permission
    redirect_to(root_path) unless BoardThreadPolicy.show?(current_user, @board_thread)
    @thread_comments = @board_thread.thread_comments.paginate(page: params[:page])
  end

  # GET /board_threads/new
  def new
    @board = Board.find(params[:board_id])
    if BoardThreadPolicy.new?(current_user, @board)
      @board_thread = @board.board_threads.build
      @form_submit_url = board_board_threads_path
    else
      redirect_to(root_path)
    end
  end

  # GET /board_threads/1/edit
  def edit
    redirect_to(root_path) unless BoardThreadPolicy.edit?(current_user, @board_thread)
    @form_submit_url = nil
  end

  # POST /board_threads
  def create
    @board = Board.find(params[:board_id])
    if BoardThreadPolicy.create?(current_user, @board)
      @board_thread = BoardThread.new(board_thread_params)
      @board_thread.user = current_user
      @board_thread.board = @board

      if @board_thread.save
        comment = ThreadComment.new
        comment.content = params[:board_thread][:thread_comment][:content]
        comment.user = @board_thread.user
        comment.board_thread = @board_thread

        if comment.save
          redirect_to @board_thread, notice: 'Board thread was successfully created.'
        else
          @board_thread.destroy
          render action: 'new'
        end
      else
        render action: 'new'
      end
    else
      redirect_to(root_path)
    end
  end

  # PATCH/PUT /board_threads/1
  # PATCH/PUT /board_threads/1.json
  def update
    if BoardThreadPolicy.update?(current_user, @board_thread)
      respond_to do |format|
        if @board_thread.update(board_thread_params)
          format.html { redirect_to @board_thread, notice: 'Thread was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @board_thread.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(root_path)
    end
  end

  # DELETE /board_threads/1
  # DELETE /board_threads/1.json
  def destroy
    if BoardThreadPolicy.destroy?(current_user, @board_thread)
      @board_thread.destroy
      respond_to do |format|
        format.html { redirect_to board_threads_url }
        format.json { head :no_content }
      end
    else
      redirect_to(root_path)
    end
  end

  # Allow a user to confirm they want to report a thread.
  def report
    redirect_to(root_path) unless BoardThreadPolicy.report?(current_user, @board_thread)
  end

  # Mark a thread for moderator review.
  def mark
    if BoardThreadPolicy.mark?(current_user, @board_thread)
      # Some threads might have null values for reported_number.
      if @board_thread.reported_number
        @board_thread.reported_number += 1
      else
        @board_thread.reported_number = 1
      end

      if @board_thread.save
        redirect_to @board_thread, notice: 'Thread was successfully marked for review.'
      else
        redirect_to report_board_thread_path(@board_thread), notice: 'Thread not marked for review.'
      end
    else
      redirect_to(root_path)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board_thread
      @board_thread = BoardThread.find(params[:id])
      @board = @board_thread.board
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def board_thread_params
      params.require(:board_thread).permit(:title, :link, :thread_comment)
    end
end
class UserLevelsController < ApplicationController
  before_action :set_user_level, only: [:show, :edit, :update, :destroy]

  # GET /user_levels
  # GET /user_levels.json
  def index
    if UserLevelPolicy.index?(current_user)
      @user_levels = UserLevel.all
    else
      redirect_to(root_path)
    end
  end

  # GET /user_levels/1
  # GET /user_levels/1.json
  def show
    redirect_to(root_path) unless UserLevelPolicy.show?(current_user, @user_level)
  end

  # GET /user_levels/new
  def new
    if UserLevelPolicy.new?(current_user)
      @user_level = UserLevel.new
    else
      redirect_to(root_path)
    end
  end

  # GET /user_levels/1/edit
  def edit
    redirect_to(root_path) unless UserLevelPolicy.edit?(current_user, @user_level)
  end

  # POST /user_levels
  # POST /user_levels.json
  def create
    if UserLevelPolicy.create?(current_user)
      @user_level = UserLevel.new(user_level_params)

      respond_to do |format|
        if @user_level.save
          format.html { redirect_to @user_level, notice: 'User level was successfully created.' }
          format.json { render action: 'show', status: :created, location: @user_level }
        else
          format.html { render action: 'new' }
          format.json { render json: @user_level.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(root_path)
    end
  end

  # PATCH/PUT /user_levels/1
  # PATCH/PUT /user_levels/1.json
  def update
    if UserLevelPolicy.update?(current_user, @user_level)
      respond_to do |format|
        if @user_level.update(user_level_params)
          format.html { redirect_to @user_level, notice: 'User level was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @user_level.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(root_path)
    end
  end

  # DELETE /user_levels/1
  # DELETE /user_levels/1.json
  def destroy
    if UserLevelPolicy.destroy?(current_user, @user_level)
      @user_level.destroy
      respond_to do |format|
        format.html { redirect_to user_levels_url }
        format.json { head :no_content }
      end
    else
      redirect_to(root_path)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_level
      @user_level = UserLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_level_params
      params.require(:user_level).permit(:name, :level, :description)
    end
end

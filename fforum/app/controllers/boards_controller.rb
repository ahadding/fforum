class BoardsController < ApplicationController
  before_action :set_board, only: [:show, :edit, :update, :destroy,
    :marked_threads, :marked_comments]

  # GET /boards
  # GET /boards.json
  def index
    if BoardPolicy.index?(current_user)
      if signed_in? && current_user.admin?
        @boards = Board.order("boards.title").paginate(page: params[:page])
      else
        @boards = Board.where(publicly_listed: true).
          order("LOWER(boards.title)").paginate(page: params[:page])
      end
    else
      redirect_to(root_path) 
    end
  end

  # GET /boards/1
  # GET /boards/1.json
  def show
    redirect_to(root_path) unless BoardPolicy.show?(current_user, @board)
    @board_threads = BoardThread.includes("thread_comments").
      where(board_id: @board.id).order("thread_comments.created_at desc").
      paginate(page: params[:page])
  end

  # GET /boards/new
  def new
    if BoardPolicy.new?(current_user)
      @board = Board.new
    else
      redirect_to(root_path)
    end
  end

  # GET /boards/1/edit
  def edit
    redirect_to(root_path) unless BoardPolicy.edit?(current_user, @board)
  end

  # POST /boards
  # POST /boards.json
  def create
    if BoardPolicy.create?(current_user)
      @board = Board.new(board_params)
      @board.user_id = current_user.id
      
      respond_to do |format|
        if @board.save
          format.html { redirect_to @board, notice: 'Board was successfully created.' }
          format.json { render action: 'show', status: :created, location: @board }
        else
          format.html { render action: 'new' }
          format.json { render json: @board.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(root_path)
    end
  end

  # PATCH/PUT /boards/1
  # PATCH/PUT /boards/1.json
  def update
    if BoardPolicy.update?(current_user, @board)
      respond_to do |format|
      if @board.update(board_params)
        format.html { redirect_to @board, notice: 'Board was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @board.errors, status: :unprocessable_entity }
      end
    end
    else
      redirect_to(root_path)
    end
  end

  # DELETE /boards/1
  # DELETE /boards/1.json
  def destroy
    if BoardPolicy.destroy?(current_user, @board)
      @board.destroy
      respond_to do |format|
        format.html { redirect_to boards_url }
        format.json { head :no_content }
      end
    else
      redirect_to(root_path)
    end
  end

  def marked_threads
    if BoardPolicy.marked_threads?(current_user, @board)
      @marked_threads = BoardThread.where.not(reported_number: nil).where.not(reported_number: 0).paginate(page: params[:page])
    else
      redirect_to board_path(@board)
    end
  end

  def marked_comments
    if BoardPolicy.marked_comments?(current_user, @board)
      @marked_comments = ThreadComment.where.not(reported_number: nil).where.not(reported_number: 0).paginate(page: params[:page])
    else
      redirect_to board_path(@board)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board
      @board = Board.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def board_params
      params.require(:board).permit(:title, :description, :publicly_listed,
        :public_read, :public_thread_create, :public_thread_reply)
    end
end

json.array!(@board_threads) do |board_thread|
  json.extract! board_thread, 
  json.url board_thread_url(board_thread, format: :json)
end

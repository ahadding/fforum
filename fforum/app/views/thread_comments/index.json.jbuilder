json.array!(@thread_comments) do |thread_comment|
  json.extract! thread_comment, 
  json.url thread_comment_url(thread_comment, format: :json)
end

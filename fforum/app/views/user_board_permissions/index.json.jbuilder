json.array!(@user_board_permissions) do |user_board_permission|
  json.extract! user_board_permission, 
  json.url user_board_permission_url(user_board_permission, format: :json)
end
